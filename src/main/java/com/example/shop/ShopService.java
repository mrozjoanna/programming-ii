package com.example.shop;

import java.util.Collection;
import java.util.HashSet;
import java.util.List;

import static java.util.stream.Collectors.toList;

public class ShopService {

    private final ShopDao dao;

    public ShopService(ShopDao dao) {
        this.dao = dao;
    }

    public void addCategory(String name) {
        dao.addCategory(new Category(name));
    }

    public Collection<Category> findAllCategories() {
        return dao.allCategories();
    }

    public List<Product> findAllProducts() {
        dao.allCategories().stream()
                .map(c -> dao.getProducts(c))
                .flatMap(products -> products.stream())
                .collect(toList());
        return null;

//        //alternatywna wersja:
//        Collection<Product> productAlternative = new HashSet<>();
//        for (Category c: dao.allCategories())
//            productAlternative.addAll(dao.getProducts(c));
//
//        return (List<Product>) productAlternative;
    }

    public void removeProductByName(String name) {
//        dao.allCategories().forEach(c -> dao.getProducts(c).removeIf(p -> p.getName().equals(name)));

        for (Category c : dao.allCategories())
            dao.getProducts(c).removeIf(p -> p.getName().equals(name));
    }

    public void removeProductBySerial(int serial) {
        for (Category c : dao.allCategories())
            dao.getProducts(c).removeIf(p -> p.getSerial() == serial);
    }

    public void addProduct(String name, String category) {

    }

    public void removeCategory(String name) {
        if (isCategoryEmpty(name))
            throw new RuntimeException("Category is not empty");
        dao.removeCategory(new Category(name));
    }

    private boolean isCategoryEmpty(String name) {
        return false;
    }
}