package com.example.shop;

import java.util.*;

public class ShopDao {
    private Map<Category, List<Product>> db = new HashMap<>();

    public void addCategory(Category category) {
        db.put(category, new ArrayList<Product>());
    }

    public void removeCategory(Category category) {
        db.remove(category);
    }

    public void addProduct(Category c, Product p) {
        db.get(c).add(p);
    }

    public void removeProduct(Category c, Product p) {
        db.get(c).remove(p);
    }

    public List<Product> getProducts(Category c) {
        return db.get(c);
    }

    public Set<Category> allCategories() {
        return db.keySet();
    }
}