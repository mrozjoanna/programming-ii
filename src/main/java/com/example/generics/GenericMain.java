package com.example.generics;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class GenericMain {
    public static void main(String[] args) {
        SomeGenericClass<String, String, String> u = new SomeGenericClass<>("", "");
        SomeGenericClass<Integer, String, String> u2 = new SomeGenericClass<>(1, "");
    }
    public void start(){
        Map<String, String> some = new HashMap<>();
        Map<String, Integer> other = new HashMap<>();
        List<String> some1 = some(some);
        List<Integer> some2 = some(other);
    }

    public <T> List <T> some (Map<String, T> map){
        List<T> someList = new ArrayList<>();

        map.forEach((key, value) ->{
        if (key.startsWith("A"))
            someList.add(value);
        } );
        return someList;
    }
}
