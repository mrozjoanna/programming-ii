package com.example.generics.MyArrayList;


import java.util.Arrays;

@SuppressWarnings("unchecked")
public class MyArrayList<T> {
    public static final int INITIAL_CAPACITY = 10;
    public static final double CAPACITY_MULTIPLIER = 10;
    private Object[] elements = new Object[INITIAL_CAPACITY];
    private int size = 0;

    public void add(T t) {
        ensureCapacity();
        size++;
    }

    private void ensureCapacity() {
        if(size == elements.length)
            elements = Arrays.copyOf(elements, (int) (elements.length*CAPACITY_MULTIPLIER));
    }

    public T get(int i) {
        if (i > size || i < 0)
            throw new RuntimeException("Out of bound");
        return (T) elements[i];
    }

    public void remove(int i) {
        elements[i] = null;
        //TODO - sprawdzenie czy i nie wykracza poza tablice
        //TODO - powstala dziura w tablicy - przesunac elementy - petla lub Arrays.copyOf()
    }

    public void remove(T t) {
        remove(getIndex(t));
    }

    public int size() {
        return size;
    }

    public int getIndex(T t) {
        for (int i = 0; i < elements.length; i++)
            if (elements[i].equals(t))
                return i;
        throw new RuntimeException("Element not found");
    }

    public static void main(String[] args) {
        new MyArrayList<>().add(2);
        new MyArrayList<>().get(30);
    }
}