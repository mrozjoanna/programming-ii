package com.example.users;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class Main {
    public static void main(String[] args) {

        List<User> users = new ArrayList();

        users.add(new User("Katarzyna", "Nowak", "99020506298", 34));
        users.add(new User("Kat", "Now", "98020507098", 32));
        users.add(new User("Tomek", "Kowalski", "90060506298", 30));
        users.add(new User("Katarzyna", "Nowak", "99020506298", 38));
        users.add(new User("Katarzyna", "Nowak", "99020506298", 34));
        users.add(new User("Katarzyna", "Nowak", "99020506298", 34));
        users.add(new User("Zofia", "Mala", "98020506298", 45));
        users.add(new User("Barbara", "Kot", "92020506298", 56));
        users.add(new User("Azor", "Kowalski", "99029506298", 29));
        users.add(new User("Azor", "Kowalski", "99029506298", 29));

        System.out.println("Array size: " + users.size());

        Set<User> noDuplicates = new HashSet<>(users);

        System.out.println("Set size: " + noDuplicates.size());
    }

}
