package com.example.users;

public class User {
    private String name;
    private String surname;
    private String pesel;
    private int age;


    public User(String name, String surname, String pesel, int age) {
        this.name = name;
        this.surname = surname;
        this.pesel = pesel;
        this.age = age;
    }

    @Override
    public boolean equals(Object o) {
        if (!(o instanceof User)) return false;
        User that = (User) o;
        return this.age == that.age
                && this.surname.equals(that.surname)
                && this.name.equals(that.name)
                && this.pesel.equals(that.pesel);
    }

    public int hashCode() {
        return age + name.hashCode() + surname.hashCode() + pesel.hashCode();
    }
}
